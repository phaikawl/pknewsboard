<?php
namespace Application\Controller;
use Zend\View\Model\ViewModel;

use Application\BaseController;
use Application\Document\Subcom;
use Application\Form\SubcomForm;
use Application\Model\SubcomInputFilterProvider;
use Application\Model\SafeUniqueInserter;

class SubcomController extends BaseController
{
    public function newAction() {
        $dm = $this->getDocumentManager();
    	$form = new SubcomForm();
        $filterProvider = new SubcomInputFilterProvider($dm);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $subcom = new Subcom;
            $form->setInputFilter($filterProvider());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $subcom->exchangeArray($form->getData());
                $subcom->prepareSave(
                    $dm,
                    $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId()
                );
        
            $inserter = new SafeUniqueInserter(
                $dm,
                $dm->getRepository("Application\Document\Subcom"),
                array(
                    'name' => $subcom->getName(),
                )
            );
            $inserter->insert($subcom);

                // Redirect to list of articles
                return $this->redirect()->toRoute('application');
            }
        }

        return array('form' => $form);
    }
}
