<?php
namespace Application\Controller;
use Zend\View\Model\ViewModel;

use Application\BaseController;
use Application\Document\Article;
use Application\Form\ArticleForm;

class ArticleController extends BaseController
{
    public function newAction() {
    	$form = new ArticleForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $article = new Article();
            $form->setInputFilter($article->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $article->exchangeArray($form->getData());
                $article->prepareSave(
                    $this->getDocumentManager(),
                    $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId()
                );
                $this->save($article);
                // Redirect to list of articles
                return $this->redirect()->toRoute('article/index');
            }
        }

        return array('form' => $form);
    }
}
