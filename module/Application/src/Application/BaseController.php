<?php
namespace Application;

use Zend\Mvc\Controller\AbstractActionController;

class BaseController extends AbstractActionController {
	/**
	 * Document manager for CouchDB
	 */
	private $dm = null;
	private $translator = null;
	
	/**
	 * Get the document manager
	 */
	function getDocumentManager() {
		if (!$this->dm) {
			$this->dm = $this->getServiceLocator()
								->get('doctrine.documentmanager.odm_default');
		}
		return $this->dm;
	}

	/**
	 * Save a document with the document manager
	 */
	function save($document) {
		$this->dm->persist($document);
		$this->dm->flush();
	}
}