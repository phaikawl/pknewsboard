<?php
namespace Application\Model;
class InsertDuplicateException extends \Exception {}

class SafeUniqueInserter
{
	private $dm = null;
	private $repo = null;
	private $spec = null;

	public function __construct($dm, $repo, $spec) {
		$this->dm = $dm;
		$this->repo = $repo;
		$this->spec = $spec;
	}

	public function insert($document) {
		$dm = $this->dm;
		$loop = $this->spec;
		foreach ($loop as $criteria => $value) {
            if ($this->repo->findOneBy(array($criteria => $value)) !== null)
            {
                throw new InsertDuplicateException;
            }
        }

        //OK
        $dm->persist($document);
        $dm->flush();

        //Check again, just being careful
        foreach ($loop as $criteria => $value) {
            $users = $this->repo->findBy(array($criteria => $value));
            if (count($users) > 1) {
                $dm->remove($document);
                $dm->flush();
                throw new InsertDuplicateException;          
            }
        }
	}
}