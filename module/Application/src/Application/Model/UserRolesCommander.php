<?php
namespace Application\Model;

use ZfcRbac\Identity\IdentityInterface;

class UserRolesCommander implements IdentityInterface
{
	private $dm = null;
	private $userid = null;
	private $roles = null;
	private $defaultRoles = array();

	/**
	 * Constructor
	 * @param int $dm the document manager
	 * @param  int $userid
	 */
	public function __construct($dm, $userid, $defaultRoles) {
		$this->dm = $dm;
		$this->userid = $userid;
		$this->defaultRoles = $defaultRoles;
		if (!$userid) {
			$this->roles = $this->defaultRoles;
		}
	}

	/**
	 * Get the roles of this user
	 * @return array
	 */
	public function getRoles() {
		if ($this->roles === null) {
			$qroles = $this->dm
				->getRepository('Application\Document\UserRole')
				->findBy(array('userid' => $this->userid));
			$roles = array();
			foreach ($qroles as $k => $role) {
				$roles[] = $role->getRole();				
			}
			$this->roles=$roles;
		}
		return $this->roles;
	}
}