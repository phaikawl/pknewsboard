<?php
namespace Application\Validator;

class Unique extends \Zend\Validator\AbstractValidator
{
    private $class = null;
    protected $repo;
    protected $criteria;

    const UNIQUE = 'pkunique';

    protected $messageTemplates = array();

    /**
     * Constructor
     * @param string $messageTemplate
     * @param Repository the document repository
     * @param array $spec
     */
    public function __construct($messageTemplate, $repo, $criteria) {
        $this->messageTemplates[self::UNIQUE] = $messageTemplate;
        $this->repo = $repo;
        $this->criteria = $criteria;
        parent::__construct();
    }

    public function isValid($value)
    {
        $this->setValue($value);
        if (!$this->itIsUnique($value)) {
            $this->error(self::UNIQUE);
            return false;
        }
        return true;
    }

    protected function itIsUnique($value) {
        $res = $this->repo->findOneBy(array($this->criteria => $value));
        if (!empty($res)) {
            return false;
        }
        return true;
        die;
    }
}