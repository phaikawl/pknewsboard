<?php
namespace Application\Document;
use Doctrine\ODM\CouchDB\Mapping\Annotations as DB;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/** @DB\Document */
class Article implements InputFilterAwareInterface
{
    /** @DB\Id */
    public $id;
    /** @DB\Field(type="string") */
    public $title;
    /** @DB\Field(type="string") */
    public $content;
    /** @DB\Field(type="string") */
    private $author;
    /** @DB\Field(type="datetime") */
    private $date;

    protected $inputFilter;  

    public function exchangeArray($data)
    {
        $this->title  = (isset($data['title']))  ? $data['title']  : null;
        $this->content = (isset($data['content']))  ? $data['content']  : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 3,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'content',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Save the article
     * @param  DocumentManager $dm the db's document manager
     */
    function prepareSave($dm, $author) {
        $this->date = new \Datetime("now");
        $this->author = $author;
    }
}