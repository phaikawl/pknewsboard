<?php
namespace Application\Document;

use Doctrine\ODM\CouchDB\Mapping\Annotations as DB;

/** @DB\Document */
class UserRole
{
	/** @DB\Id */
	public $id=null;
	/**
	 * @DB\Index
	 * @DB\Field(type="string")
	 */
	public $userid=null;
	/** @DB\Field(type="string") */
	public $role=null;

	/**
	 * Constructor
	 * @param int $userid
	 */
	public function __construct($userid) {
		$this->userid=$userid;
	}

	/**
	 * Get role
	 *
	 * @return string
	 */
	public function getRole()
	{
	    return $this->role;
	}
	
	/**
	 * Set role
	 *
	 * @param string $role
	 * @return
	 */
	public function setRole($role)
	{
	    $this->role = $role;
	    return $this;
	}
}