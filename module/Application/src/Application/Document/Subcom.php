<?php
namespace Application\Document;
use Doctrine\ODM\CouchDB\Mapping\Annotations as DB;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;   

/** @DB\Document */
class Subcom implements InputFilterAwareInterface
{
    /** @DB\Id */
    public $id;
    /**
     * @DB\Field(type="string")
     * @DB\Index
     */
    public $name;
    /** @DB\Field(type="string") */
    public $title;
    /** @DB\Field(type="string") */
    public $founder;
    /** @DB\Field(type="string") */
    public $desc;
    /** @DB\Field(type="datetime") */
    public $date;

    protected $inputFilter;

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    

    public function exchangeArray($data)
    {
        $this->title  = (isset($data['title']))  ? $data['title']  : null;
        $this->desc = (isset($data['desc']))  ? $data['desc']  : null;
        $this->name = (isset($data['name']))  ? $data['name']  : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        $this->inputFilter = $inputFilter;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            throw new \Exception("InputFilter has not been set");
        }

        return $this->inputFilter;
    }

    /**
     * Save the article
     * @param  DocumentManager $dm the db's document manager
     */
    function prepareSave($dm, $founder) {
        $this->date = new \Datetime("now");
        $this->founder = $founder;
    }
}