<?php

namespace Application\Mapper;

use Doctrine\ODM\CouchDB\DocumentManager,
    Application\Options\ModuleOptions,
    Application\Model\SafeUniqueInserter;

class UserInsertDuplicateException extends \Exception {
    public function __construct($message = null) {
        if (!$message) {
            $this->message = "Duplicated username or email.";
        }
    }
}

class UserCouchDB implements \ZfcUser\Mapper\UserInterface
{
    /**
     * @var \Doctrine\ODM\DocumentManager
     */
    protected $dm;

    /**
     * @var \Application\Model\SafeUniqueInserter
     */
    protected $inserter;
    
    /**
     * @var \ZfcUserDoctrineORM\Options\ModuleOptions
     */
    protected $options;
    
    public function __construct(DocumentManager $dm, ModuleOptions $options)
    {
        $this->dm      = $dm;
        $this->options = $options;
        $this->entityClass = $this->options->getUserEntityClass();
    }

    public function findByEmail($email)
    {
        $user = $this->getUserRepository()->findOneBy(array('email' => $email));
        return $user;
    }

    public function findByUsername($username)
    {
        $user = $this->getUserRepository()->findOneBy(array('username' => $username));
        return $user;
    }
    
    public function findById($id)
    {
        $user = $this->dm->find($this->entityClass, $id);
        return $user;
    }

    public function getDocumentManager()
    {
        return $this->dm;
    }

    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
        return $this;
    }

    public function getUserRepository()
    {
        return $this->dm->getRepository($this->entityClass);
    }
    
    public function persist($document)
    {
        $dm = $this->dm;
        $dm->persist($document);
        $dm->flush();
    }
    
    public function insert($document, $tableName = null, HydratorInterface $hydrator = null)
    {
        if (!$this->inserter) {
            $this->inserter = new SafeUniqueInserter(
                $this->dm,
                $this->getUserRepository(),
                array(
                    'username' => $document->getUsername(),
                    'email' => $document->getEmail()
                )
            );
        }
        $this->inserter->insert($document);
    }

    public function update($document, $where = null, $tableName = null, HydratorInterface $hydrator = null)
    {
        if (!$where) {
            $where = 'id = ' . $document->getId();
        }

        $this->dm->persist($document);
        $this->dm->flush();
    }
}
