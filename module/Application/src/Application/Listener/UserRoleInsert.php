<?php
namespace Application\Listener;
use Application\Document\UserRole;
class UserRoleInsert
{
	/**
     * @param $e event
     */
    public static function onRegister($e) {
    	$user = $e->getParam('user');
    	$dm = $e->getTarget()->getServiceManager()->get('dmservice');
    	$roleModel = new UserRole($user->getId());
    	$roleModel->setRole('member');
    	$dm->persist($roleModel);
        $dm->flush();
    }
}