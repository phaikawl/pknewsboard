<?php
namespace Application\Form;
use Zend\Form\Form;

class ArticleForm extends Form {
	public function __construct($name = null) {
        parent::__construct();
        // we want to ignore the name passed
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name'  => 'content',
            'type'          => 'Zend\Form\Element\Textarea',
            'options'       => array(
                'label'             => 'Content',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Submit',
                'id' => 'submitbutton',
            ),
        ));
    }
}