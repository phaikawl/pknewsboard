<?php
namespace ApplicationTest;

class UserServiceStub {
	public function __construct($realUserService) {
		$this->realService = $realUserService;
	}

	public function register($user) {
		$realService = $this->realService;
		$realService->getEventManager()->trigger(__FUNCTION__, $this, array('user' => $user));
        $realService->getUserMapper()->insert($user);
        $realService->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('user' => $user));
        return $user;
	}

	public function getUserClass() {
		return $this->realService->getOptions()->getUserEntityClass();
	}

	public function getUserMapper() {
		return $this->realService->getUserMapper();
	}

	public function getServiceManager() {
		return $this->realService->getServiceManager();
	}
}