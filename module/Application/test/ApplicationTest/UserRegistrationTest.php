<?php
namespace ApplicationTest;
use ApplicationTest\Bootstrap;
use ApplicationTest\UserServiceStub;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class UserRegistrationTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $this->setApplicationConfig(
            include '../../../config/application.config.php'
        );

        $this->sm = Bootstrap::getServiceManager();
        $this->dm = $this->sm->get('doctrine.documentmanager.odm_default');
    }

    public function testRegister() {
        // $data = array(
        //     'email' => "testing22@gmail.com",
        //     'username' => 'testing22',
        //     'password' => 'Imsuperman22',
        // );
        $userService = new UserServiceStub($this->sm->get("zfcuser_user_service"));
        $cls = $userService->getUserClass();
        $user = new $cls;
        $user->setEmail('testing22@gmail.com');
        $user->setUsername('testing22');
        $user->setPassword('Imsuperman22');
        $user = $userService->register($user);
        $this->user = $user;
        $repo = $userService->getUserMapper()->getUserRepository();
        $this->repo=$repo;
        $qusers = $repo->findBy(array('username' => $user->getUsername()));
        // $this->assertTrue(count($qusers) === 1);

        //Test the user role insert injection
        $qroles = $this->dm->getRepository("Application\Document\UserRole")
        ->findBy(array('userid' => $user->getId()));
        $this->roles=$qroles;
        $this->assertTrue(count($qroles) > 0);
    }

    public function tearDown() {
        $this->dm->remove($this->user);
        $this->dm->flush();
        foreach ($this->roles as $role) {
             $this->dm->remove($role);
        }
    }
}