<?php
/**
 * ZfcRbac Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$settings = array(
    /**
     * The default role that is used if no role is found from the
     * role provider.
     */
    'anonymousRole' => 'guest',

    /**
     * Flag: enable or disable the routing firewall.
     */
    'firewallRoute' => false,

    /**
     * Flag: enable or disable the action resource firewall.
     */
    'firewallActionResource' => true,

    /**
     * Flag: enable or disable the controller firewall.
     */
    'firewallController' => false,

    /**
     * Set the view template to use on a 403 error.
     */
    'template' => 'error/403',

    /**
     * flag: enable or disable the use of lazy-loading providers.
     */
    'enableLazyProviders' => false,

    'firewalls' => array(
        'ZfcRbac\Firewall\ActionResource' => array(
            'Application\Controller\Article' => array(
                'new' => 'article.create',
                'edit' => 'article.edit.own',
            ),
            'Application\Controller\Subcom' => array(
                'new' => 'subcom.create',
                'edit' => 'subcom.edit.own',
            ),
        ),
    ),

    'providers' => array(
        'ZfcRbac\Provider\Generic\Role\InMemory' => array(
            'roles' => array(
                'admin',
                'member' => array('admin'),
                'guest' => array('member'),
            ),
        ),
        'ZfcRbac\Provider\Generic\Permission\InMemory' => array(
            'permissions' => array(
                'admin' => array('admin'),
                'member' => array('article.create', 'subcom.create'),
            )
        ),
    ),

    /**
     * Set the identity provider to use. The identity provider must be retrievable from the
     * service locator and must implement \ZfcRbac\Identity\IdentityInterface.
     */
    'identity_provider' => 'application_identity'
);

$serviceManager = array(
    'factories' => array(
        'application_identity' => function ($sm) {
            $dm = $sm->get('doctrine.documentmanager.odm_default');
            $authService = $sm->get('zfcuser_auth_service');
            $userid=null;
            if ($authService->hasIdentity()) {
                $userid = $authService->getIdentity()->getId();
            }
            $identity = new Application\Model\UserRolesCommander($dm, $userid, array('guest'));
            return $identity;
        },
    )
);

/**
 * You do not need to edit below this line
 */
return array(
    'zfcrbac' => $settings,
    'service_manager' => $serviceManager,
);
