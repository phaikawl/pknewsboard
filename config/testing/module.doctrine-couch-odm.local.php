<?php
return array(
    'doctrine' => array(

        'connection' => array(
            'odm_default' => array(
                'type'      => 'socket',
                'host'      => 'localhost',
                'port'      => '5984',
                'user'      => null,
                'password'  => null,
                'dbname'    => 'testdb',
                'ip'        => '127.0.0.1',
                'options'   => array()
            ),
        ),

        'configuration' => array(
            'odm_default' => array(
                'metadata_cache'     => 'array',

                'driver'             => 'odm_default',

                'generate_proxies'   => true,
                'proxy_dir'          => 'data/DoctrineCouchODMModule/Proxy',
                'proxy_namespace'    => 'DoctrineCouchODMModule\Proxy',

                'default_db'         => null,

                'filters'            => array(),  // array('filterName' => 'BSON\Filter\Class'),

                'logger'             => null // 'DoctrineMongoODMModule\Logging\DebugStack'
            )
        ),

        'documentmanager' => array(
            'odm_default' => array(
                'connection'    => 'odm_default',
                'configuration' => 'odm_default',
                'eventmanager' => 'odm_default'
            )
        ),

        'eventmanager' => array(
            'odm_default' => array(
                'subscribers' => array()
            )
        ),
    ),
);
